# Grid Operation

Due to the limitation of achievable hardware versatility in existing energy simulation laboratories, the digital networking of different laboratories of specialized competences is a promising approach for allowing higher simulated system complexity, combining the expertise of different working groups and a heterogeneous experimental environment. Within the framework of the research project “Future laboratory Digitalization Energy” (ZLE), the laboratories of the University of Applied Sciences Emden/Leer (HS–Emden/Leer) and of the German Aerospace Center (DLR-VE) - Institute of Networked Energy Systems have been connected for first co-simulation tests. The prospective objective is the investigation of the digital and increasingly decentral controlled supply of flexibility in low-voltage (LV) grids in the context of robust grid operation management.


## Table of Contents
- [Objectives](#objectives)
- [Installation Instructions](#installinginstr)
- [Co-Simulation Structure and Models](#structure)
- [Example Results](#results)
- [Citation](#keyreferences)
- [Authors & Acknowledgments](#authorsackn)
  

## <a name="objectives"></a> Objectives
- The current objective is to couple two energy laboratories (emulations/ simulations) of different infrastructures with the help of an applicable co-simulation  framework.
- The software architecture should provide a fast communication with a low latency to support subsequent investigations of fast events in LV grids.
- The hardware infrastructure should meet the requirements of typical representative electrical grids, which facilitates the comparison of measurement results and technologies for grid optimization.
- Adequate tools and user interfaces should be provided to control and monitor the hardware and properly couple numerical simulations.
- Prospectively, various sub-models from further partners will be coupled in a single co-simulation environment to realize a collaborative research platform in lower Saxony.


## <a name="installinginstr"></a> Installation Instructions
Since real hardware is used in addition to the numerical simulations, a large part of the network structure can also be represented by real components or power amplifiers. This can be done according to your own possibilities and requirements. As a result, the results may differ greatly from the values shown here.

**1. Matlab Simulink**

Follow the instructions of your organnisation or use the guide on the official [Mathworks website](https://de.mathworks.com/help/install/install-products.html)
Prerequisites & Toolboxes
- MATLAB/ Simulink
- Simscape Electrical
- Simulink Real-Time	

**2. WireGuard**

Installation instructions for your operating system can be found on the official [WireGuard website](https://www.wireguard.com/install/)

**3. VILLASnode** 

VILLASnode is provided by the Institute for Automation of Complex Power Systems (ACS) EON Energy Research Center (EONERC) RWTH University Aachen, Germany under a GPLv3 license. [link](https://www.fein-aachen.org/projects/villas-node/)
Follow the installation instructions for your operating system on the official [VILLASframework Documentation website](https://villas.fein-aachen.org/docs/installation)



## <a name="structure"></a> Co-Simulation Structure and Models

### Coupling of two labs - Providing flexibility with a battery storage system
The experiment was performed at two facilities. The DLR_NESTEC of the DLR Insitute of Networked Energy Systems and Laboratory of Renewable Energies (RE-Lab) of the University of Applied Sciences Emden/Leer

For investigations in the field of grid optimization, a comprehensive evaluation and clustering of real grid data into so-called basic grid topologies is advantageous for a scenario analysis and a holistic and cross-system evaluation. In this way, influences can be systematically investigated and their effect on the application of grid optimization measures could be quantified. For these reasons, the basic topology of the LV grid MONA type I (Figure 1), which consists of 21 nodes, 6 lines of 728 m total length and a transformer was selected as a test grid for the applied simulations. In ths case the low voltage grid was divided into three parts.

|<img src="docs/MONA-coupling.png" width=500>|
|:--:|
|**Figure 1:** The electrical grid is divided into three areas which are performed in simulation or real hardware.|

In order to validate the laboratory coupling approach, two different scenarios were considered (see Figure 2). For each scenario, results  from a monolithic numerical simulation of the MONA grid are used as reference. The goal of the scenarios is to test the feasibility of the (P)HiL geographically separated co-simulation for flexibility studies including controls and devices in the laboratories presented in Section. For both scenarios, the RTS in Oldenburg has a time-step of 100 $\mu s$. The measured values at the nodes at the RE-Lab are read at 5 Hz. At the Gridlab, the measured values are recorded at 10 kHz in the RTS. The hardware measurements are recorded at 20 kHz.

|<img src="docs/scenarios.png" width=500>|
|:--:|
|**Figure 2:** Two scenarios with flexibility options at different locations where defined|

The conceptual communication structure of the cosimulation of the two laboratories is shown in Figure 3. The communication between the two locations takes place over the internet using a virtual private network (VPN). The communication and data exchange of signals is bidirectional using the UDP protocol. At each laboratory, a communication gateway for the simulators is employed in order to decouple communication and simulation layers in the experiments. As a communication gateway, the VILLAS frame work was selected. In DLR_NESTEC Gridlab, the simulator is a RTS with a separate VILLAS server, whereas in the RE-Lab, the simulator is an instance of a Matlab App using a virtual machine (VM) as gateway.

|<img src="docs/DLR-HS-EL_-_communication.png" width=500>|
|:--:|
|**Figure 3:** Communication structure between the two labs.|


### Coupling of three labs - Sector coupling with a heat pump as flexibility

In step two the integration of a third lab (geographically seoarated) is used to incorporate the heating sector into flexibilty provision in the form of a heat pump. This is located in the heating laboratory of the Ostfalia University of Applied Sciences and was integrated into the electrical grid as a further consumer (see figure 4).

The objectiv is now to test a platform for the §14a EnWG case. To see an meaningful effect in grid stability, the voltage is coupled with residual load at the transformer. An increasing load will lead to an decreasing voltage. In the first step the batterx storage system in Emden will provide power infeed to stabilize the voltage to the normal level. However, if the maximum output of the battery storage unit is exceeded, the heat pump must be reduced to the legally stipulated output of 4,2 kW as a further measure.

|<img src="docs/MONA_1.png" width=500>|
|:--:|
|**Figure 4:** The electrical grid and its participants is distributed over the three research facilities.|


The heating laboratory of the Ostfalia University of Applied Sciences was integrated into VILLASnode as an additional node for this purpose. A special fact here is that the controller for the heat pump (WAGO PLC) communicates directly with the VILLASnode computer.

|<img src="docs/ZLE_Communication_structure.png" width=500>|
|:--:|
|**Figure 5:** Communication structure between the three labs.|


## <a name="results"></a> Example Results

### Coupling of two labs - Providing flexibility with a battery storage system


|<img src="Results/Figures/sc1_inv_power_voltage_hsel.png " width=500>|
|:--:|
|**Figure 6:** Comparison of ideal voltage behavior from simulation to a real device.|

Figure 6 presents a comparison between the experimental results obtained from the co-simulation experiment with the Real-Time Simulator (RTS) and the simulated results from the reference numerical simulation of the MONA grid model. The figure depicts the power consumption at point CP1 and the voltage at the Point of Common Coupling (PCC). A significant discrepancy is observed between the simulated values and the experimental data. This highlights that differences in hardware setups, configurations, and even timing and latency issues can introduce time shifts between simulators, leading to errors that may not be immediately apparent. These discrepancies underscore the importance of considering the specific characteristics of each simulator and experimental setup when comparing results. 


### Coupling of three labs - Sector coupling with a heat pump as flexibility

The scenario under investigation was a §14a EnWG dimming need due to grid instabilities. Specifically, in this case, the threshold for undervoltage triggered an automated shutdown of the heat pump located at the Ostfalia Heating Laboratory to bring overall voltagelevel to a normal level.
Before that dimming as a last measure, the battery storage system located in the Renewable Energy Laboratory attempts to stabilize the voltage by injecting power into the grid.
To simulate the decreasing voltage level in the grid, a steadily increasing load was applied. In real-world scenarios, this could also be triggered by a reduction in photovoltaic (PV) feed-in within a highly utilized grid section.

|<img src="Results/Figures/2024-11_ZLE_pu_anot.png" width=500>|
|:--:|
|**Figure 7:** Flexibility of components as measure to avoid dimming according to §14a EnWG.|

Figure 7 illustrates the behavior of the heat pump and battery storage system in response to changing voltage levels across different phases. All power values are expressed in per unit (pu) notation. The base values used for normalization are as follows: 25 kW for the heat pump and 3.3 kW for the battery storage system.

Initially, during Phase 1, no load is applied. At the onset of Phase 2, a hardware load is activated in the grid model in Emden, resulting in a minor voltage drop. Subsequently, at the end of Phase 2, an additional larger load is turned on at DLR_NESTEC, leading to a more pronounced voltage drop to approximately 225 V. The battery storage system's threshold for activation was set to 229 V, and as shown, it begins to feed power into the grid once this threshold is reached. During Phase 3, the ongoing voltage drop continued unabated despite the increasing power output from the battery storage system. Ultimately, it was not possible to halt the voltage decline until the fictional grid operator sent a shutdown signal for the heat pump at a voltage level of 214 V.Following the shutdown of the heat pump, there is an immediate partial recovery of the voltage level. However, the normal voltage level cannot be fully restored. The battery storage system continues its efforts to stabilize the grid by attempting to further increase power infeed.


## <a name="keyreferences"></a> Key References
##### If you want to cite this repository, please use the following citation:
S. Fayed, A. Rubio, J. Petznik, J. Rolink and F. Schuldt, "Infrastructure of a Laboratory Coupled Co-simulation for the Investigation of Flexibility Provision in Distribution Grids," ETG Congress 2023, Kassel, Germany, 2023, pp. 1-9. 

## <a name="authorsackn"></a> Authors & Acknowledgments
### Repository Owner
- [Jan Petznik](mailto:jan.petznik@dlr.de)

### Additional Contributions
- [Alejandro Rubio](mailto:Alejandro.Rubio@dlr.de)
- [Sarah Fayed](mailto:sarah.fayed@hs-emden-leer.de)
- [Ihsan Ünal](mailto:i.uenal@ostfalia.de)


### Funding and Acknowledgments
This research was funded by the Lower Saxony Ministry of Science and Culture under grant number 11-76251-13-3/19 ZN3488 (ZLE) within the Lower Saxony “SPRUNG“ of the Volkswagen Foundation. It was supported by the Center for Digital Innovations (ZDIN).

