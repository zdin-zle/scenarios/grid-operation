# VILLASnode

## About 
VILLASnode is a flexible gateway for co-simulation interface data. It converts protocols, (de-)multiplexed signals to and from multiple sources and destinations. Currently, about 18 different protocols and interfaces are supported, ranging from common broker-based messaging protocols like AMQP and MQTT, to IEC 61850, to web protocols like WebSockets or FIWARE NGSI, and many more. It collects statistics on exchanged data such as packet loss, one-way delay and jitter, and provides network simulation to simulate the circumstances of a geographically distributed co-simulation. VILLASnode is a C/C++ program optimized for real-time Linux operating systems.[^1]

## Interface

This is VILLASnode, a gateway for processing and forwardning simulation data between real-time simulators.
VILLASnode is a client/server application to connect simulation equipment and software such as:

- OPAL-RT RT-LAB,
- RTDS GTFPGA cards,
- RTDS GTWIF cards,
- Simulink,
- LabView,
- and FPGA models

by using protcols such as:

- IEEE 802.2 Ethernet / IP / UDP,
- ZeroMQ & nanomsg,
- MQTT & AMQP
- WebSockets
- Shared Memory
- Files
- IEC 61850 Sampled Values / GOOSE
- Analog/Digital IO via Comedi drivers
- Infiniband (ibverbs)


|<img src="docs/VILLASnode_interfaces.png" width=600>|
|:-:|
|**Figure 1:** Interfaces[^2]|


## Documentation

User documentation can be found here: 
- <https://villas.fein-aachen.org/doc/node.html> 
- <https://villas.fein-aachen.org/docs/node/>

## Download the software

VILLASnode is distributed in several ways.Source Code (GitLab), package manager (Debian, Redhat), or Docker Images.
For more Information visit the downloadpage:
- [https://www.fein-aachen.org/downloads/]

## Licensing

VILLASnode is released as open source software under the GPLv3 license. Other licensing options are available upon request.[^1] <img src="https://www.fein-aachen.org/img/logos/gplv3.png" width=50>

<br>
<br>

#### References
[^1]: https://www.fein-aachen.org/projects/villas-node/
[^2]: https://www.fein-aachen.org/img/VILLASnode_interfaces.png
